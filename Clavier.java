import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.layout.TilePane;
import javafx.scene.shape.Circle ;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import javax.swing.ButtonGroup;

/**
 * Génère la vue d'un clavier et associe le contrôleur aux touches
 * le choix ici est d'un faire un héritié d'un TilePane
 */
public class Clavier extends TilePane{
    /**
     * il est conseillé de stocker les touches dans un ArrayList
     */
    private List<Button> clavier;
    private MotMystere modelePendu;

    /**
     * constructeur du clavier
     * @param touches une chaine de caractères qui contient les lettres à mettre sur les touches
     * @param actionTouches le contrôleur des touches
     * @param tailleLigne nombre de touches par ligne
     */
    public Clavier(String touches, EventHandler<ActionEvent> actionTouches, MotMystere modelePendu, List<Button> clavier) {
        this.modelePendu = modelePendu;
        List<String> alpha = Arrays.asList("a", "b", "c");
        int i = 0 ;
        while(i<27){
            Button alphabet = new Button(alpha.get(i));
            alphabet.setOnAction(actionTouches);
            this.clavier.add(alphabet);

            i++;
        }
    }

    

    // /**
    //  * permet de désactiver certaines touches du clavier (et active les autres)
    //  * @param touchesDesactivees une chaine de caractères contenant la liste des touches désactivées
    //  */
    // public void desactiveTouches(Set<String> touchesDesactivees){
    //     for (int i : touchesDesactivees){
    //         this.modelePendu.getLettresEssayees().add(touchesDesactivees.get(i));
    //     }
    // }
}
